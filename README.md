# Trailer APP
This application provides trailers to customers. It takes a movie resource as a query parameter and returns a link to the trailer of that movie.

## **Running the application from the localhost:**
 *Note: This application was built with an environment which has Node version 8.10.0 and NPM version 6.0.1*
 
1.  ``git clone git@bitbucket.org:ugunebakan/trailerapp.git``
2. ``cd trailerapp``
3. ``npm install``
4. ``export AUTH_KEY = ***themoviedb_key***``
5. ``npm start``
6. Now you can access the application from localhost:3000

## **Running the application from a docker container:**
1.  ``git clone git@bitbucket.org:ugunebakan/trailerapp.git``
2. ``docker built . -t trailerapp``
3. ``docker run -p 0.0.0.0:8080:3000/tcp -e AUTH_KEY= ***themoviedb_key*** trailerapp``
4. Now you can access the application from localhost:8080

## How to run the tests:
1.  ``git clone git@bitbucket.org:ugunebakan/trailerapp.git``
2. ``cd trailerapp``
3. ``npm install``
4. ``export AUTH_KEY = ***themoviedb_key***``
5. ``npm test``


## **API Methods:**

|METHOD          | PATH                          | QUERY PARAMS                         |
|----------------|-------------------------------|-----------------------------|
|GET         |`/api/trailerfinder`            |`?link=http://movie.resource.com` |

**Success Response (200 OK):**

```
{
  link:  "https://www.youtube.com/watch?v=tFMo3UJ4B4g"
}
```
**Error Responses:**

 - If link query parameter is not available:

  Request: GET /api/findTrailer?a=https://content.viaplay.se/pc-se/film/arrival-2016
  Response: 400 Bad Request
  ```
  {
    message:  "link query parameter is missing"
  }
  ```

 - If link is invalid:

  Request: GET /api/findTrailer?a=https://content.viaplay.se/pc-se/film/arrival-2016
  Response: 400 Bad Request
  ```
  {
    message:  "invalid link"
  }
  ```

## Caching:
I implemented an in memory cache to decrease the response time of each request. For the first time it takes ~1000 seconds for a specific movie resource, then it decreases to 15 seconds. For a larger project, products like **Redis** or **Memcache** can be used and container pods must auto scale. 


## Environmental Variables:
This application gets the trailer link from themoviedb.org and to get the trailer, AUTH_KEY environmental variable must be set. Otherwise, application will return 401 Unauthorized:
```
{
    "status_code": 7,
    "status_message": "Invalid API key: You must be granted a valid key."
}
```
