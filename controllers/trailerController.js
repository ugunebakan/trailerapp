'use strict';
const request = require('request-promise');

const authKey = process.env.AUTH_KEY || 'noAuthKey';
// initial cache
const cacheArray = [];
const cache = {
  url: '',
  link: ''
};

function getYouTubeLink(imdbId) {
  const movieServiceUrlTemplate = `https://api.themoviedb.org/3/movie/${encodeURIComponent(imdbId)}/videos?api_key=${encodeURIComponent(authKey)}&external_source=imdb_id`;
  //send request to the themoviedb site
  return request(movieServiceUrlTemplate)
    .then((data) => {
      const result = JSON.parse(data).results;

      // find the first object in array which has a type Trailer and site Youtube
      const trailerId = result.filter((video) => {
        return video.type === 'Trailer' && video.site === 'YouTube';
      })[0];

      // if there is a trailer ID, it returns the youtube link
      if (trailerId) {
        const trailerLink = `https://www.youtube.com/watch?v=${encodeURIComponent(trailerId.key)}`;

        // add link to the cache object then push it to the cache array
        cache.link = trailerLink;
        cacheArray.push(cache);

        return trailerLink;
      }
    });
}

module.exports = (url) => {

  // checks the cache for trailers
  const filteredCache = cacheArray.filter((cacheObj) => {
    return cacheObj.url === url;
  })[0];

  // if there is a cache it will return it
  if (filteredCache) return Promise.resolve(filteredCache.link);

  // write url for new cache entry
  cache.url = url;

  return request(url)
    .then((data) => {
      const imdbObj = JSON.parse(data);
      const imdbId = imdbObj._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id;

      return imdbId;
    })
    .then(getYouTubeLink);
};
