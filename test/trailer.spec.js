/* eslint-env mocha */
'use strict';
const request = require('supertest');

const app = require('../server.js');
const assert = require('assert');


describe('GET /api/findTrailer API End Point Tests', () => {

  it('should return 200 and give correct data for correct link', (done) => {
    request(app)
      .get('/api/findTrailer?link=https://content.viaplay.se/pc-se/film/arrival-2016')
      .expect((res) => {
        throw assert.equal(200, res.statusCode);
      })
      .expect((res) => {
        throw assert.deepEqual(res.body, { link: 'https://www.youtube.com/watch?v=tFMo3UJ4B4g' });
      })
      .end(done);
  });

  it('should return 400 for a query with out a link query parameter', (done) => {
    request(app)
      .get('/api/findTrailer?hoho=https://content.viaplay.se/pc-se/film/arrival-2016')
      .expect((res) => {
        throw assert.equal(400, res.statusCode);
      })
      .end(done);
  });

  it('should return 400 for invalid links', (done) => {
    request(app)
      .get('/api/findTrailer?hoho=a')
      .expect((res) => {
        throw assert.equal(400, res.statusCode);
      })
      .end(done);
  });

});
