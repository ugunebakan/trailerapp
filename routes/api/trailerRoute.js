'use strict';
const express = require('express');
const router = new express.Router();
const validate = require('url-validator');

const findTrailer = require('../../controllers/trailerController.js');

router.route('/')
  .get((req, res) => {
    //check if request has a query parameter as link
    if (!('link' in req.query)) {
      res.status(400).json({message: 'link query parameter is missing'});
    } else {
      // check if the link is a valid link
      if (!validate(req.query.link)) {
        res.status(400).json({message: 'invalid link'});
      } else {
        // validations are OK, app logic
        const url = req.query.link;
        findTrailer(url)
          .then((trailerLink) => res.json({link: trailerLink}))
          .catch((err) => {
            res.status(err.statusCode).json(JSON.parse(err.response.body));
          });
      }
    }
  });

module.exports = router;
