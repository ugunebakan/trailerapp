'use strict';
const express = require('express');
const router = new express.Router();

router.use('/findtrailer', require('./trailerRoute'));

module.exports = router;
