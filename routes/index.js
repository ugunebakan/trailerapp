'use strict';
const express = require('express');
const router = new express.Router();

//api routes
router.use('/api', require('./api'));

module.exports = router;
