'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./routes/index.js');

// application routes
app.use(routes);

// json parser
app.use(bodyParser.json());

// not to clash instance ports
if (require.main === module) {
  app.listen(3000, () => {});
}

// for testing purposes
module.exports = app;
